<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<footer class="main-footer">
  <div class="footer-left">
    Copyright &copy; <?=date('Y')?> <div class="bullet"></div> Tabungan Siswa
  </div>
  <div class="footer-right">

  </div>
</footer>
</div>
</div>

<?php $this->load->view('_partials/js');?>